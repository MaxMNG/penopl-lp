$(function() {

	// Инициализация WOW and Animate 
	new WOW().init();

  // Инициализация карусельки контента
  $('.content_types').owlCarousel({
    loop: true,
    items: 3,
    center: true,
    smartSpeed: 700,
    nav: false,
    dots: false,
    autoHeight: false,
    autoplay: true,
  });

  $('.cool-carousel').owlCarousel({
    loop: true,
    items: 1,
    smartSpeed: 700,
    nav: true,
    navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    dots: true,
    autoHeight: false,
    autoplay: true,
    autoplayTimeout: 8000,
  });

  $('.blogers').owlCarousel({
    loop: true,
    items: 1,
    margin: 150,
    center: true,
    smartSpeed: 700,
    nav: false,
    dots: true,
    autoHeight: false,
    responsiveClass: true,
    responsive: {
      1200: {
        items: 2
      }
    }
  });

  // Menu black
  $(window).scroll(function() {
    if ($(this).scrollTop() > $(this).height()-500) {
      $('.top_menu').addClass('active');
    } else {
      $('.top_menu').removeClass('active');
    }
  });

  // UP-button
  $(window).scroll(function() {
    if ($(this).scrollTop() > $(this).height()) {
      $('.top').addClass('active');
    } else {
      $('.top').removeClass('active');
    }
  });

  // UP-button click
  $('.top').click(function() {
    $('html,body').stop().animate({scrollTop: 0});
  });

  $('.logo_link').click(function() {
    $('html,body').stop().animate({scrollTop: 0}, 'slow', 'swing');
  });


  // Работа меню
  $(document).ready(function(){
    $(".landing_menu").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('html,body').animate({scrollTop: top-74}, 'slow', 'swing');
    });
  });

  //Прелоадер
  $(window).on('load', function() {
    $('.preloader').delay(1000).fadeOut('slow');
  });








});
